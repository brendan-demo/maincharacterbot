require('dotenv').config();
require('./lib/prereqs');

const twit = require('./lib/twitter');
const db = require('./lib/db');
db.connect();

const fullTweet = false;

//checkTwitter();

async function checkTwitter() {
  const mentions = await twit
    .getMentions(fullTweet)
    .catch((err) => console.error(err));

  console.log(mentions);
}
