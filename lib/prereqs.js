if (!process.env.CONSUMER_KEY) throw new Error('CONSUMER_KEY not specified');

if (!process.env.CONSUMER_SECRET)
  throw new Error('CONSUMER_SECRET not specified');

if (!process.env.ACCESS_TOKEN_KEY)
  throw new Error('ACCESS_TOKEN_KEY not specified');

if (!process.env.ACCESS_TOKEN_SECRET)
  throw new Error('ACCESS_TOKEN_SECRET not specified');

if (!process.env.DATABASE_URL) throw new Error('DATABASE_URL not specified');
